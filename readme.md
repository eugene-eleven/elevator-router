# Elevator router

### Current implementation of elevator router consists of such entities as
- **dispatcher**. Is a wrapper above elevators. In real life is a controller
that installed in building and communicate directly with every elevator
- **elevator**. Is an entity that represents state and expose some api
to manage a physical aggregate — lift.
- **route_optimizer**. Algorithm that pick which elevator to delegate a call
from certain floor. 
- **events**. Message broker that deliver messages from dispatcher
installed in building to BE application.

### Install

Mainly you only need to install packages 
```bash
npm ci
```

Then you can take a look in test framework how it works
```bash
npm t
```

### Description
To describe an architecture lets take a look at one of tests in 
[load_emilation.test.ts](./tests/load_emulation.test.ts).

```typescript
it("should serve single call from single elevator on ground floor", async () => {
    const elevators: Elevator[] = [new Elevator(1, 1000)];
    const events = new RouterEvents();
    const dispatcher = new Dispatcher(events, elevators, 28);
    const fakeElevator = new FakeElevator(dispatcher, events);
    fakeElevator.schedule([
        [
            "lobby_called",
            {
                level: 10,
                target: 0,
            },
            10,
        ],
    ]);

    await fakeElevator.run();

    await fakeElevator.stopElevators();

    expect(dispatcher.getHistory()).toMatchObject({
        1: createExpectedElevatorPathThruFloors([0, 10, 0]),
    });
});
```

To run server application we need to init `Dispatcher`, which requires event
broker `RouterEvents` and list of `Elevators`. 
Then, to make it live, we have to init `FakeElevator`, a class that mimics
elevators events via `RouterEvents`. Later once we call `fakeElevator.run` it
update elevator state every `N` seconds and react to "calls" of passengers.

To emulate passenger's action there is method:
```ts
fakeElevator.schedule([
        [
            "lobby_called",
            {
                level: 10,
                target: 0,
            },
            10,
        ],
    ]);
```
It delays an event of elevator call at floor 10.

Then we call `fakeElevator.stopElevators()`, which stops elevators after second.
During this time all actions should already have been finished.

Finally, method
```ts
expect(dispatcher.getHistory()).toMatchObject({
        1: createExpectedElevatorPathThruFloors([0, 10, 0]),
    });
```
Checks that elevator with id `1` travelled from level `0` to `10` and back.

### There are more complex and descriptive examples available in [tests](./tests).

### Algorithm that pick which elevator to assign to certain call implemented and described in [route_optimizer.ts](./app/router/route_optimizer.ts)

