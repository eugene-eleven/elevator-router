import {
  AllowedRouteEvent,
  RouterEvents,
  Dispatcher,
  ElevatorState,
} from "./router";

export class FakeElevator {
  private msDelay: number = 10;
  private readonly scheduledSignals: [keyof AllowedRouteEvent, any, number][] =
    [];
  private elevatorHeartBit: NodeJS.Timeout;

  constructor(
    private readonly dispatcher: Dispatcher,
    private readonly events: RouterEvents,
  ) {}

  schedule<T extends keyof AllowedRouteEvent>(
    payload: [T, AllowedRouteEvent[T][0], number][],
  ) {
    this.scheduledSignals.push(...payload);
  }

  async run() {
    this.aliveElevators();

    for (const [eventType, payload, delay] of this.scheduledSignals) {
      await new Promise((resolve) =>
        setTimeout(resolve, delay || this.msDelay),
      );
      this.events.emit(eventType, payload);
    }
  }

  public async stopElevators(inMs: number = 1000) {
    await new Promise((resolve) => setTimeout(resolve, inMs));
    clearInterval(this.elevatorHeartBit);
  }

  private aliveElevators() {
    this.elevatorHeartBit = setInterval(() => {
      this.dispatcher.elevators.forEach((elevator) => {
        if (elevator.getState() === ElevatorState.idle) {
          return;
        }
        const elevatorFloor = elevator.getFloor();
        const isEndOfRun =
          (elevator.getState() === ElevatorState.down &&
            elevatorFloor <= this.dispatcher.minLevel) ||
          (elevator.getState() === ElevatorState.up &&
            elevatorFloor >= this.dispatcher.maxLevel);

        if (!isEndOfRun) {
          this.events.emit("elevator_reached_level", {
            id: elevator.id,
            level:
              elevatorFloor +
              (elevator.getState() === ElevatorState.down ? -1 : 1),
          });
        }
      });
    }, this.msDelay);
  }
}
