import { Elevator, ElevatorState, ElevatorStatus } from "./elevator";

const calculateCoefficientOfRunningOutElevator = (elevator: ElevatorStatus) => {
  const floorsTravelOppositeToCaller = elevator.floorsToTravelAtThisRun.length
    ? Math.abs(
        elevator.floor -
          Math[elevator.state === ElevatorState.up ? "max" : "min"](
            ...elevator.floorsToTravelAtThisRun,
          ),
      )
    : 0;
  return -(
    // waiting time depends on amounts of stops
    (
      10 * elevator.floorsToTravelAtThisRun.length +
      // and floors to travel opposite to you and back
      20 * floorsTravelOppositeToCaller
    )
  );
};

const isSameWayAsCallerNeed = (
  elevator: {
    floorDistance: number;
    state: ElevatorState;
  },
  direction: number,
): boolean => {
  if (elevator.floorDistance === 0) {
    return true;
  }
  if (direction < 0 && elevator.state === ElevatorState.down) {
    return true;
  }
  return direction > 0 && elevator.state === ElevatorState.up;
};

const movesInDirectionOfCaller = (
  elevator: {
    floor: number;
    state: ElevatorState;
  },
  targetLevel: number,
): boolean => {
  if (elevator.floor < targetLevel && elevator.state === ElevatorState.up) {
    return true;
  }
  return elevator.floor > targetLevel && elevator.state === ElevatorState.down;
};

/**
 * Inspired by
 * @link https://softwareengineering.stackexchange.com/a/331694
 *
 * This method accepts list of elevators, source and target floors.
 * To define which elevator to schedule, code iterates over elevators
 * and calculate what the distance elevator should travel from now
 * to reach source floor. If target floor is against of elevator direction,
 * current elevator route added to travel length.
 *
 * Then we select elevator with lowest distance to travel scheduled.
 * */
export const pickElevator = (
  elevators: Elevator[],
  sourceFloor: number,
  targetFloor: number,
): Elevator => {
  let maxFloorDistance = 0;
  const positions = elevators
    .map((elevator, i) => {
      const floorDistance = elevator.getFloor() - sourceFloor;
      const absoluteFloorDistance = Math.abs(floorDistance);
      if (absoluteFloorDistance > maxFloorDistance) {
        maxFloorDistance = absoluteFloorDistance;
      }
      return {
        ...elevator.getStatus(),
        floorDistance,
        absoluteFloorDistance,
        i,
      };
    })
    .filter((stat) => !stat.full)
    .map((elevator) => {
      const isMoveInDirectionOfCaller = movesInDirectionOfCaller(
        elevator,
        sourceFloor,
      );
      const sameWayAsCallerNeed = isSameWayAsCallerNeed(
        elevator,
        targetFloor - sourceFloor,
      );

      // absolute distance to elevator from caller
      const closeCoefficient =
        10 * (Math.max(maxFloorDistance, 1) / elevator.absoluteFloorDistance);

      // travel that elevator have to do before it heads required direction
      const routeRunCoefficient =
        elevator.state === ElevatorState.idle ||
        (sameWayAsCallerNeed && isMoveInDirectionOfCaller)
          ? 0
          : calculateCoefficientOfRunningOutElevator(elevator);

      // summary coefficient.
      const k = routeRunCoefficient + closeCoefficient;

      return {
        ...elevator,
        k: k,
      };
    })
    .reduce((elevatorA, elevatorB) => {
      if (elevatorA.k === elevatorB.k) {
        return elevatorB.load > elevatorA.load ? elevatorB : elevatorA;
      }
      return elevatorB.k > elevatorA.k ? elevatorB : elevatorA;
    });
  return elevators[positions.i];
};
