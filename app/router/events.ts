import { EventEmitter } from "node:events";
import { ElevatorState } from "./elevator";

export interface ElevatorReachedLevelEvent {
  /**
   * Elevator Id
   * */
  id: number;

  /**
   * Number of level, elevator reach at this moment
   * */
  level: number;
}

export interface ElevatorStateChanged {
    /**
     * Elevator Id
     * */
    id: number;

    /**
     * New weight
     * */
    load: number;

    /**
     * New state
     * */
    state: ElevatorState;
}

export interface ElevatorDestinationAdded {
    /**
     * Elevator Id
     * */
    id: number;

    /**
     * Destination requested
     * */
    target: number;
}

export interface LobbyCall {
  /**
   * Level at which elevator was called
   * */
  level: number;

  /**
   * Destination level
   * */
  target: number;
}


export type AllowedRouteEvent = {
  lobby_called: [LobbyCall];
  elevator_reached_level: [ElevatorReachedLevelEvent];
  elevator_destination_added: [ElevatorDestinationAdded],
  elevator_state_changed: [ElevatorStateChanged];
};

export class RouterEvents extends EventEmitter<AllowedRouteEvent> {}
