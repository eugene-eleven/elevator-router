export enum ElevatorState {
  "idle",
  "up",
  "down",
}

export interface ElevatorStatus {
  full: boolean;
  floorsToTravelAtThisRun: number[];
  floor: number,
  load: number,
  state: ElevatorState,
  id: number,
}

/**
 * The main idea of approach: elevator run in single direction until it reaches last floor
 * Then in run in opposite direction, if there are floors scheduled
 * */
export class Elevator {
  /**
   * How loaded should be elevator not to serve any more calls
   * */
  private readonly overloadThreshold: 0.8;

  private state: ElevatorState = ElevatorState.idle;
  private floor: number = 0;
  private load: number = 0;
  private scheduledFloors: Set<number> = new Set();
  private oppositeDirectionScheduledFloors: Set<number> = new Set();

  constructor(
    public readonly id: number,
    private readonly maxWeight: number,
  ) {}

  setFloor(value: number) {
    this.floor = value;

    if (this.scheduledFloors.has(value)) {
      this.scheduledFloors.delete(value);
    }
    if (!this.scheduledFloors.size) {
      this.runEnd();
    }

    return this;
  }

  getFloor() {
    return this.floor;
  }

  setState(value: ElevatorState) {
    this.state = value;

    return this;
  }

  getState() {
    return this.state;
  }

  setLoad(value: number) {
    this.load = value;

    return this;
  }

  runEnd() {
    this.setState(ElevatorState.idle);
    if (this.oppositeDirectionScheduledFloors.size) {
      this.scheduledFloors = this.oppositeDirectionScheduledFloors;
      this.oppositeDirectionScheduledFloors = new Set();
    }
    if (this.scheduledFloors.size) {
      this.startRun();
    }
  }

  startRun() {
    const average =
        Array.from(this.scheduledFloors).reduce((a, b) => a + b) /
        this.scheduledFloors.size;
    this.setState(
        average > this.floor ? ElevatorState.up : ElevatorState.down,
    );
  }

  addDestination(value: number) {
    if (this.state === ElevatorState.idle) {
      this.scheduledFloors.add(value);
    } else if (this.state === ElevatorState.up && value > this.floor) {
      this.scheduledFloors.add(value);
    } else if (this.state === ElevatorState.down && value < this.floor) {
      this.scheduledFloors.add(value);
    } else {
      this.oppositeDirectionScheduledFloors.add(value);
    }

    return this;
  }

  getStatus(): ElevatorStatus {
    return {
      full: this.load >= this.maxWeight * this.overloadThreshold,
      floorsToTravelAtThisRun: Array.from(this.scheduledFloors),
      floor: this.floor,
      load: this.load,
      state: this.state,
      id: this.id,
    };
  }
}
