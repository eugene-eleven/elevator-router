import {
  ElevatorDestinationAdded,
  ElevatorReachedLevelEvent,
  ElevatorStateChanged,
  LobbyCall,
  RouterEvents,
} from "./events";
import { Elevator, ElevatorState, ElevatorStatus } from "./elevator";
import { pickElevator } from "./route_optimizer";

export class Dispatcher {
  private readonly elevatorsHash: Record<number, Elevator>;
  private readonly elevatorsArray: Elevator[];
  private readonly elevatorHistory: {
    [elevatorId: number]: number[];
  };
  constructor(
    private readonly eventSource: RouterEvents,
    public readonly elevators: Elevator[],
    public readonly maxLevel: number,
    public readonly minLevel: number = 0,
  ) {
    const elevatorIds = elevators.map((elevator) => elevator.id);
    if (elevatorIds.length !== new Set(elevatorIds).size) {
      throw new Error("Elevator ids should be unique");
    }

    this.elevatorsHash = Object.fromEntries(
      elevators.map((elevator) => [elevator.id, elevator]),
    );
    this.elevatorsArray = elevators;
    this.elevatorHistory = Object.fromEntries(
      elevators.map((elevator) => [elevator.id, [elevator.getFloor()]]),
    );

    this.subscribeListeners();
  }

  elevatorStateChanged({ load, state, id }: ElevatorStateChanged) {
    this.validateElevator(id);
    const elevator = this.elevatorsHash[id].setLoad(load);
    if (state === ElevatorState.idle) {
      elevator.runEnd();
    } else {
      elevator.setState(state);
    }
  }

  elevatorReachedLevel({ level, id }: ElevatorReachedLevelEvent) {
    this.validateLevels(level);
    this.validateElevator(id);
    this.elevatorsHash[id].setFloor(level);
    this.elevatorHistory[id].push(level);
  }

  elevatorDestinationAdded({ target, id }: ElevatorDestinationAdded) {
    this.validateLevels(target);
    this.validateElevator(id);
    this.elevatorsHash[id].addDestination(target);
  }

  lobbyCalled({ level, target }: LobbyCall) {
    this.validateLevels(level, target);

    const elevator = pickElevator(this.elevatorsArray, level, target);
    elevator.addDestination(level);
    if (elevator.getState() === ElevatorState.idle) {
      elevator.startRun();
    }
    elevator.addDestination(target);
  }

  getState(): ElevatorStatus[] {
    return this.elevatorsArray.map((elevator) => elevator.getStatus());
  }

  getHistory() {
    return this.elevatorHistory;
  }

  private validateElevator(elevatorId: number) {
    if (!this.elevatorsHash[elevatorId]) {
      console.warn(`Insufficient elevator provided ${elevatorId}.`, {
        allowed: Object.keys(this.elevatorsHash),
      });
      throw new Error("Insufficient elevator provided");
    }
  }

  private validateLevels(...levels: number[]) {
    const invalidLevels = levels.filter(
      (level) => level > this.maxLevel || level < this.minLevel,
    );
    if (invalidLevels.length) {
      console.warn(
        `invalid level(s) provided ${invalidLevels.join(", ")}. Allowed: ${this.minLevel} - ${this.maxLevel}`,
      );
      throw new Error("Invalid level");
    }
  }

  private subscribeListeners() {
    this.eventSource.on("lobby_called", this.lobbyCalled.bind(this));
    this.eventSource.on(
      "elevator_reached_level",
      this.elevatorReachedLevel.bind(this),
    );
    this.eventSource.on(
      "elevator_destination_added",
      this.elevatorDestinationAdded.bind(this),
    );
    this.eventSource.on(
      "elevator_state_changed",
      this.elevatorStateChanged.bind(this),
    );
  }
}
