import { Elevator, ElevatorState } from "../app/router/elevator";
import { pickElevator } from "../app/router/route_optimizer";

describe("route_optimizer", () => {
  describe("idle cases", () => {
    it("should pick first elevator if all conditions are same", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000),
        new Elevator(2, 1000),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[0]);
    });

    it("should pick first elevator if they placed at same direction", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(2),
        new Elevator(2, 1000).setFloor(8),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[0]);
    });

    it("should pick elevator that located closer below", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(3),
        new Elevator(2, 1000).setFloor(4),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[1]);
    });

    it("should pick elevator that located closer above", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(8),
        new Elevator(2, 1000).setFloor(7),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[1]);
    });

    it("should pick elevator that located closer absolutely", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(0),
        new Elevator(2, 1000).setFloor(7),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[1]);
    });

    it("should pick elevator if it stays idle on caller floor, instead of one above", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(5),
        new Elevator(2, 1000).setFloor(6),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[0]);
    });

    it("should pick elevator if it stays idle on caller floor, instead of one belov", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(5),
        new Elevator(2, 1000).setFloor(6),
      ];
      expect(pickElevator(elevators, 6, 10)).toBe(elevators[1]);
    });
  });

  describe("cases when elevator run in direction of caller", () => {
    it("should pick elevator that run in your direction from below instead of in other", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(4)
          .setState(ElevatorState.up)
          .addDestination(8),
        new Elevator(2, 1000)
          .setFloor(4)
          .setState(ElevatorState.down)
          .addDestination(2),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[0]);
    });

    it("should pick elevator that run in your direction from above instead of in other", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(6)
          .setState(ElevatorState.up)
          .addDestination(8),
        new Elevator(2, 1000)
          .setFloor(6)
          .setState(ElevatorState.down)
          .addDestination(2),
      ];
      expect(pickElevator(elevators, 5, 1)).toBe(elevators[1]);
    });

    it("should pick elevator that closer to caller from above", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(10)
          .setState(ElevatorState.down)
          .addDestination(8),
        new Elevator(2, 1000)
          .setFloor(20)
          .setState(ElevatorState.down)
          .addDestination(2),
      ];
      expect(pickElevator(elevators, 5, 10)).toBe(elevators[0]);
    });

    it("should pick elevator that closer to caller from below", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(1)
          .setState(ElevatorState.up)
          .addDestination(8),
        new Elevator(2, 1000)
          .setFloor(5)
          .setState(ElevatorState.up)
          .addDestination(9),
      ];
      expect(pickElevator(elevators, 10, 15)).toBe(elevators[1]);
    });
  });

  describe("cases when elevator run out of caller", () => {
    it("should pick first elevator since it travel less amount of floors up before get to caller", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(11)
          .setState(ElevatorState.up)
          .addDestination(18),
        new Elevator(2, 1000)
          .setFloor(16)
          .setState(ElevatorState.up)
          .addDestination(26),
      ];

      expect(pickElevator(elevators, 10, 15)).toBe(elevators[0]);
    });

    it("should pick first elevator since it travel less amount of floors down before get to caller", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(18)
          .setState(ElevatorState.down)
          .addDestination(11),
        new Elevator(2, 1000)
          .setFloor(16)
          .setState(ElevatorState.down)
          .addDestination(3),
      ];

      expect(pickElevator(elevators, 20, 8)).toBe(elevators[0]);
    });

    it("should pick second elevator since it travel less amount of floors before get to caller", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(18)
          .setState(ElevatorState.down)
          .addDestination(11),
        new Elevator(2, 1000)
          .setFloor(23)
          .setState(ElevatorState.up)
          .addDestination(25),
      ];

      expect(pickElevator(elevators, 20, 10)).toBe(elevators[1]);
    });

    it("should pick second elevator since it has faster route that first, considering stops, direction up", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(18)
          .setState(ElevatorState.down)
          .addDestination(13)
          .addDestination(11),
        new Elevator(2, 1000)
          .setFloor(23)
          .setState(ElevatorState.up)
          .addDestination(30),
      ];

      expect(pickElevator(elevators, 20, 10)).toBe(elevators[1]);
    });

    it("should pick second elevator since it has faster route that first, considering stops, direction down", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(23)
          .setState(ElevatorState.up)
          .addDestination(25)
          .addDestination(30),
        new Elevator(2, 1000)
          .setFloor(18)
          .setState(ElevatorState.down)
          .addDestination(11),
      ];

      expect(pickElevator(elevators, 20, 10)).toBe(elevators[1]);
    });

    it("should pick first elevator since it has shorter route then second, which passing thru but in other direction, direction down", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(12)
          .setState(ElevatorState.up)
          .addDestination(15),
        new Elevator(2, 1000)
          .setFloor(8)
          .setState(ElevatorState.up)
          .addDestination(26),
      ];

      expect(pickElevator(elevators, 10, 0)).toBe(elevators[0]);
    });

    it("should pick second elevator since it has shorter route then second, which passing thru but in other direction, direction up", () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000)
          .setFloor(12)
          .setState(ElevatorState.down)
          .addDestination(2),
        new Elevator(2, 1000)
          .setFloor(8)
          .setState(ElevatorState.down)
          .addDestination(4),
      ];

      expect(pickElevator(elevators, 10, 20)).toBe(elevators[1]);
    });
  });
});
