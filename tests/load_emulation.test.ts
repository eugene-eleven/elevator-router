import {
  Elevator,
  ElevatorState,
  Dispatcher,
  RouterEvents,
} from "../app/router";
import { FakeElevator } from "../app/fake_elevator";

const createExpectedElevatorPathThruFloors = (floors: number[]): number[] => {
  return floors.reduce(
    (passed, target) => {
      const start = passed.pop();
      const k = target - start > 0 ? 1 : -1;
      for (let floor = start; floor !== target; floor += k) {
        passed.push(floor);
      }
      passed.push(target);
      return passed;
    },
    [floors.shift()],
  );
};

describe("emulates usage scenarios for daily usage", () => {
  it("check that dispatcher init successfully", () => {
    const elevators: Elevator[] = [
      new Elevator(1, 1000),
      new Elevator(2, 1000),
    ];
    const events = new RouterEvents();
    const dispatcher = new Dispatcher(events, elevators, 28);
    expect(dispatcher).toBeDefined();
    expect(dispatcher.getState()).toMatchObject([
      {
        full: false,
        floorsToTravelAtThisRun: [],
        floor: 0,
        load: 0,
        state: ElevatorState.idle,
        id: 1,
      },
      {
        full: false,
        floorsToTravelAtThisRun: [],
        floor: 0,
        load: 0,
        state: ElevatorState.idle,
        id: 2,
      },
    ]);
  });

  it("should serve single call from single elevator on ground floor", async () => {
    const elevators: Elevator[] = [new Elevator(1, 1000)];
    const events = new RouterEvents();
    const dispatcher = new Dispatcher(events, elevators, 28);
    const fakeElevator = new FakeElevator(dispatcher, events);
    fakeElevator.schedule([
      [
        "lobby_called",
        {
          level: 10,
          target: 0,
        },
        10,
      ],
    ]);

    await fakeElevator.run();

    await fakeElevator.stopElevators();

    expect(dispatcher.getHistory()).toMatchObject({
      1: createExpectedElevatorPathThruFloors([0, 10, 0]),
    });
  });

  it("should serve single call from one of elevators on ground floor", async () => {
    const elevators: Elevator[] = [
      new Elevator(1, 1000),
      new Elevator(2, 1000),
    ];
    const events = new RouterEvents();
    const dispatcher = new Dispatcher(events, elevators, 28);
    const fakeElevator = new FakeElevator(dispatcher, events);
    fakeElevator.schedule([
      [
        "lobby_called",
        {
          level: 10,
          target: 0,
        },
        10,
      ],
    ]);

    await fakeElevator.run();
    await fakeElevator.stopElevators();

    expect(dispatcher.getHistory()).toMatchObject({
      1: createExpectedElevatorPathThruFloors([0, 10, 0]),
    });
  });

  it("should queue elevator stop to elevator that have less distance to travel, and then add a stop during the route", async () => {
    const elevators: Elevator[] = [
      new Elevator(1, 1000)
        .setFloor(8)
        .setState(ElevatorState.down)
        .addDestination(0),
      new Elevator(2, 1000)
        .setFloor(19)
        .setState(ElevatorState.up)
        .addDestination(24),
    ];
    const events = new RouterEvents();
    const dispatcher = new Dispatcher(events, elevators, 28);
    const fakeElevator = new FakeElevator(dispatcher, events);
    fakeElevator.schedule([
      [
        "lobby_called",
        {
          level: 10,
          target: 0,
        },
        10,
      ],
      [
        "lobby_called",
        {
          level: 14,
          target: 0,
        },
        30,
      ],
    ]);

    await fakeElevator.run();
    await fakeElevator.stopElevators();

    expect(dispatcher.getHistory()).toMatchObject({
      1: createExpectedElevatorPathThruFloors([8, 0]),
      2: createExpectedElevatorPathThruFloors([19, 24, 0]),
    });
  });

  it(
    "elevator that drives down collect all passengers that are driving down, meantime elevator in " +
      "opposite direction takes its passengers",
    async () => {
      const elevators: Elevator[] = [
        new Elevator(1, 1000).setFloor(27),
        new Elevator(2, 1000).setFloor(1),
      ];
      const events = new RouterEvents();
      const dispatcher = new Dispatcher(events, elevators, 28);
      const fakeElevator = new FakeElevator(dispatcher, events);
      fakeElevator.schedule([
        [
          "lobby_called",
          {
            level: 27,
            target: 0,
          },
          10,
        ],
        [
          "lobby_called",
          {
            level: 4,
            target: 8,
          },
          10,
        ],

        [
          "lobby_called",
          {
            level: 8,
            target: 1,
          },
          0,
        ],
        [
          "lobby_called",
          {
            level: 7,
            target: 9,
          },
          10,
        ],
        [
          "lobby_called",
          {
            level: 18,
            target: 22,
          },
          10,
        ],
        [
          "lobby_called",
          {
            level: 13,
            target: 25,
          },
          10,
        ],
      ]);
      await fakeElevator.run();
      await fakeElevator.stopElevators();

      expect(dispatcher.getHistory()).toMatchObject({
        1: createExpectedElevatorPathThruFloors([27, 0]),
        2: createExpectedElevatorPathThruFloors([1, 25]),
      });
    },
  );
});
